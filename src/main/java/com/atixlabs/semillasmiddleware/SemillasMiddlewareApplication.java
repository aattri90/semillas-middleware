package com.atixlabs.semillasmiddleware;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemillasMiddlewareApplication {

	public static void main(String[] args) {
		SpringApplication.run(SemillasMiddlewareApplication.class, args);
	}

}
